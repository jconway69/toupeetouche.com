.EXPORT_ALL_VARIABLES:
IMAGE = "jconway69/arm-javascript"
TAG := $(shell uname -m)
STACK = javascript
all: build

build:
	@docker build -t ${IMAGE}:$(TAG) -f Dockerfile_${TAG} .

push:
	@docker push ${IMAGE}:$(TAG)

test:
	@docker-compose up -d

deploy:
	@docker stack deploy --with-registry-auth -c docker-stack.yml $(STACK)

clean:
	-docker-compose down

.PHONY: all build push test deploy clean

